from mycroft import MycroftSkill, intent_file_handler

import os
import openai

# openai.api_key = os.getenv("OPENAI_API_KEY")


class Aitalk(MycroftSkill):
    def __init__(self):
        MycroftSkill.__init__(self)
        self.last_message = ''
        openai.api_key = self.settings.get('api_key', '')

    @intent_file_handler('aitalk.intent')
    def handle_aitalk_code(self, message):
        content = message.data.get('content').strip()
        prompt = "The following is a QA with an AI assistant. The assistant is helpful, creative, clever, and very friendly.It is also great at coding.\nAI: I am an AI with a lot of data. You can ask me questions, ask for help or we can just talk!\nHuman: How a graphic card works?\nAI: A graphic card processes and renders images, video, and other graphics for display on a computer monitor. It does this by using a system of chips and circuits that convert digital information into an analog signal that can be displayed on a screen.\nHuman: Whats the book clean code talk about?\nAI: Clean Code is a book about writing good code. It is written by Robert C. Martin and published by Prentice Hall in 2008.\nHuman: Can you recommend another book related to this?\nAI: I think you might be interested in this book: The Pragmatic Programmer, from Journeyman to Master by Andrew Hunt, David Thomas\nHuman: how can i get the type for a variable in python?\nAI: There are two ways to get types in Python. The first way is to use the built-in type ( ) function. >>> foo = \"Hello World!\" >>> type ( foo ) <type 'str'> The other way is to use the isinstance ( ) function. You can use this method if you want to check whether or not an object is an instance of a certain class\nHuman: What is a static function in typescript?\nAI: A static function in TypeScript is a function that is associated with a class, but not with an instance of the class. Static functions are called without creating an instance of the class.\nHuman: Write an interface in typescript for a dog with name, breed, color and age.\nAI:\ninterface Dog {\n  name: string;\n  breed: string;\n  color: string;\n  age: number;\n}\nHuman: how can i trim a string in python?\nAI: You can use the strip ( ) function to remove leading and trailing whitespace from a string. >>> foo = \" Hello World! \" >>> foo.strip ( ) 'Hello World!'\nHuman: "

        if (message.data.get('utterance').find('also') > -1 or message.data.get('utterance').find('continuing') > -1):
            prompt += self.lastMessage + '\nHuman: ' + content + "?"
        else:
            prompt += content + "?"

        print('aitalk: ', prompt)

        response = openai.Completion.create(
            model="code-davinci-002",
            prompt=prompt,
            temperature=0.75,
            max_tokens=150,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0.6,
            stop=[" Human:", "Human: "]
        )
        self.lastMessage = content + response.choices[0].text
        print('response: ', response)
        self.speak_dialog('aitalk', data={
            'response': response.choices[0].text.replace('AI:', ''),
            'question': content
        }, expect_response=True)

    @intent_file_handler('aitalk.chat.intent')
    def handle_aitalk(self, message):
        content = message.data.get('content').strip()
        prompt = "The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.\n\nHuman: Hello, who are you?\nAI: I am an AI with a lot of data. You can ask me questions, ask for help or we can just talk! \nHuman: How a graphic card works?\nAI: A graphic card processes and renders images, video, and other graphics for display on a computer monitor. It does this by using a system of chips and circuits that convert digital information into an analog signal that can be displayed on a screen.\nHuman: Whats the story of harry potter?\nAI: The story of Harry Potter follows the adventures of a young wizard, Harry Potter, as he navigates his way through life at Hogwarts School of Witchcraft and Wizardry, battles the evil Lord Voldemort, and tries to uncover the truth about his family.\nHuman: What is anarchism?\nAI: Anarchism is a political philosophy that advocate for the creation of a society without government or rulers.\nHuman: Give me 3 books to understand anarchism\nAI: \"Anarchism: What It Really Stands For\" by Albert Meltzer, \"Anarchism: Its Philosophy and Scientific Basis\" by Peter Kropotkin, and \"Anarchy, State, and Utopia\" by Robert Nozick.\nHuman: "
        if (message.data.get('utterance').find('also') > -1 or message.data.get('utterance').find('continuing') > -1):
            prompt += self.lastMessage + '\nHuman: ' + content + "?"
        else:
            prompt += 'Talk ' + content + "?"

        print('aitalk: ', prompt)

        response = openai.Completion.create(
            model="text-davinci-002",
            prompt=prompt,
            temperature=0.9,
            max_tokens=150,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0.6,
            stop=[" Human:", " AI:"]
        )
        self.lastMessage = content + response.choices[0].text
        print('response: ', response)
        self.speak_dialog('aitalk', data={
            'response': response.choices[0].text.replace('AI:', ''),
            'question': content
        }, expect_response=True)


def create_skill():
    return Aitalk()
